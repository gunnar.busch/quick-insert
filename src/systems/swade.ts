// Savage Worlds Adventure Edition integration
import { CharacterSheetContext } from "../module/contexts";
import { QuickInsert } from "../module/core";
import { getSetting, setSetting } from "../module/settings";
import { ModuleSetting } from "../module/store/ModuleSettings";

import type { SearchItem } from "module/searchLib";

export const SYSTEM_NAME = "swade";

export const defaultSheetFilters = {
  skill: "swade.skills",
  hindrance: "swade.hindrances",
  edge: "swade.edges",
  ability: "",
  weapon: "",
  armor: "",
  shield: "",
  gear: "",
  "character.choice": "",
  "vehicle.choice": "",
  mod: "",
  "vehicle-weapon": "",
};

export class SwadeSheetContext extends CharacterSheetContext {
  equipped = false;
  constructor(
    documentSheet: DocumentSheet,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string,
    equipped?: boolean
  ) {
    super(documentSheet, anchor);
    this.equipped = Boolean(equipped);
    if (sheetType && insertType) {
      const sheetFilters = getSetting(ModuleSetting.FILTERS_SHEETS).baseFilters;
      this.filter =
        sheetFilters[`${sheetType}.${insertType}`] || sheetFilters[insertType];
    }
  }
  onSubmit(item: SearchItem | string): Promise<AnyDocument[]> | undefined {
    const res = super.onSubmit(item);
    if (this.equipped && res) {
      res.then((items) => {
        const item = items.length && items[0];
        if (!item) return;

        //@ts-ignore
        if (item?.data?.equippable) {
          item.update({ "data.equipped": true });
        }
      });
    }

    return res;
  }
}

export function sheetSwadeRenderHook(
  app: DocumentSheet,
  sheetType?: string
): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }

  // Legacy sheets
  const link = `<a class="quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  app.element.find("a.item-create").each((i, el) => {
    const type = el.dataset.type || "";
    const equipped = el.dataset.equipped === "true";
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new SwadeSheetContext(
        app,
        linkEl,
        sheetType,
        type,
        equipped
      );
      QuickInsert.open(context);
    });
  });

  // New character sheet
  app.element.find("button.item-create").each((i, el) => {
    const type = el.dataset.type || "";
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new SwadeSheetContext(app, linkEl, sheetType, type);
      QuickInsert.open(context);
    });
  });
}

export function init(): void {
  if (game.user?.isGM) {
    const customFilters = getSetting(ModuleSetting.FILTERS_SHEETS).baseFilters;
    setSetting(ModuleSetting.FILTERS_SHEETS, {
      baseFilters: {
        ...defaultSheetFilters,
        ...customFilters,
      },
    });
  }
  Hooks.on("renderCharacterSheet", (app: DocumentSheet) => {
    getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED) &&
      sheetSwadeRenderHook(app, "character");
  });
  Hooks.on("renderSwadeNPCSheet", (app: DocumentSheet) => {
    getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED) &&
      sheetSwadeRenderHook(app, "npc");
  });
  Hooks.on("renderSwadeVehicleSheet", (app: DocumentSheet) => {
    getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED) &&
      sheetSwadeRenderHook(app, "vehicle");
  });

  console.log("Quick Insert | swade system extensions initiated");
}
